import { css } from '@emotion/react';
import type { NextPage } from 'next';
import { useEffect } from 'react';
import Header from '../components/Header';
import MovieCard from '../components/MovieCard';
import { MovieProps, MoviesData } from '../interfaces';
import { moviesActions, useAppDispatch, useAppSelector } from '../redux';
import { theme } from '../theme';

const Home: NextPage = () => {
  const dispatch = useAppDispatch();
  const moviesState = useAppSelector((state) => state.movies) as unknown as { fetchData: MoviesData };

  useEffect(() => {
    dispatch(moviesActions.fetch());
  }, [])

  return (
    <div css={styles.root}>
      <div css={styles.body}>
        {moviesState.fetchData && moviesState.fetchData.allMovies.nodes.map((movie: MovieProps) => {
          return (
            <MovieCard 
              key={movie.id} 
              id={movie.id} 
              title={movie.title} 
              releaseDate={movie.releaseDate} 
              reviewAuthor={movie.userByUserCreatorId.name} 
              imgUrl={movie.imgUrl} 
            />
          )
        })}
      </div>
    </div>
  );
};

const styles = {
  root: css({
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: theme.palette.grey[600],
  }),
  body: css({
    alignSelf: 'stretch',
    padding: 32,
    paddingBottom: 0,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }),
};

export default Home;