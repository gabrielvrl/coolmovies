import { gql, useMutation, useQuery } from "@apollo/client";
import { css } from "@emotion/react";
import { Button, Typography, useTheme } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import TextInputComponent from "../../components/TextInputComponent";
import { theme } from "../../theme";
import { GET_ALL_MOVIES_REVIEWS, MovieReviewProps } from ".././review/[review]";

const CREATE_MOVIE_REVIEW = gql`
  mutation CreateMovieReview($input: CreateMovieReviewInput!) {
    createMovieReview(input: $input) {
      clientMutationId
    }
  }
`;

const CreateMovieReview = () => {
  const [title, setTitle] = useState<string>('');
  const [rating, setRating] = useState<string>('');
  const [body, setBody] = useState<string>('');
  const [createMovieReview] = useMutation(CREATE_MOVIE_REVIEW);
  const theme = useTheme();
  const router = useRouter();
  const { CreateMovieReview } = router.query;
  const { refetch } = useQuery<MovieReviewProps>(GET_ALL_MOVIES_REVIEWS);

  const handleCreateMovieReview = async (event: FormEvent) => {
    event?.preventDefault();

    if(!rating) return;

    await createMovieReview({
      variables: {
        input: {
          movieReview: {
            title,
            rating: parseInt(rating),
            body,
            movieId: CreateMovieReview,
            userReviewerId: "7b4c31df-04b3-452f-a9ee-e9f8836013cc", // I left Marle user as the reviewer for all reviews for now since I don't have a login system yet
          }
        }
      },
      onError: (error) => {
        console.log('error', error);
      },
      onCompleted: () => {
        refetch();
        router.push(`/`)
      }
    });
  };

  return (
    <form css={styles.root} onSubmit={handleCreateMovieReview}>
      <div css={styles.body}>
        <Typography
          sx={{
            fontSize: '2rem',
            fontWeight: 'bold',
            color: theme.palette.primary.light,
            marginBottom: '1rem',
            height: 'fit-content',
          }}
        >Add Movie Review</Typography>

        <TextInputComponent 
          state={title}
          setState={setTitle}
          label='Title of your review'
          id='review-title'
        />

        <TextInputComponent 
          state={rating}
          setState={setRating}
          label='Your rating'
          id='rating'
        />

        <TextInputComponent 
          state={body}
          setState={setBody}
          label='Your Movie Review'
          id='movie-review'
          isMovieReviewData
        />

        <Button variant="contained" type="submit">Submit</Button>

      </div>
    </form>
  );
};

const styles = {
  root: css({
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.grey[600],
  }),
  body: css({
    alignSelf: 'flex-start',
    width: 'fit-content',
    padding: 32,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  }),
};

export default CreateMovieReview;