import { gql, useMutation, useQuery } from '@apollo/client';
import { css } from '@emotion/react';
import { Box, Button, Modal, Typography, useTheme } from '@mui/material';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import MovieCard from '../../components/MovieCard';
import { moviesActions, RootState, useAppDispatch } from '../../redux';
import PersonIcon from '@mui/icons-material/Person';
import Image from 'next/image';
import TextInputComponent from '../../components/TextInputComponent';
import { theme } from '../../theme';


interface MovieProps {
  title: string;
  userByUserReviewerId: {
    name: string;
  }
  movieId: number;
  rating: number;
  body: string;
  id: number;
  movieByMovieId: {
    id: string;
    imgUrl: string;
    movieDirectorId: number;
    releaseDate: string;
    title: string;
    userByUserCreatorId: {
      name: string;
      id: string;
    }
  }
}

export interface MovieReviewProps {
  allMovieReviews: {
    edges: MovieProps[];
  }
}

export const GET_ALL_MOVIES_REVIEWS = gql`
  query MyQuery {
    allMovieReviews {
      edges {
        node {
          title
          userByUserReviewerId {
            name
          }
          movieId
          rating
          body
          id
          movieByMovieId {
            id
            imgUrl
            movieDirectorId
            releaseDate
            title
            userByUserCreatorId {
              name
              id
            }
          }
        }
      }
    }
  }
`
const UPDATE_MOVIE_REVIEW_BY_ID = gql`
  mutation UpdateMovieReviewById($input: UpdateMovieReviewByIdInput!) {
    updateMovieReviewById(input: $input) {
      clientMutationId
    }
  }
`;

const MovieReviews: NextPage = () => {
  const router = useRouter();
  const { review } = router.query;
  const dispatch = useAppDispatch();
  const movieState = useSelector((state: RootState) => state.movies.movieProps);
  const [movie, setMovie] = useState<MovieProps>({} as MovieProps);
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [rating, setRating] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [editReviewId, setEditReviewId] = useState('');
  const theme = useTheme();

  const handleOpen = () => setOpenModal(true);
  const handleClose = () => setOpenModal(false);

  const [updateMovieReviewById] = useMutation(
    UPDATE_MOVIE_REVIEW_BY_ID
  );

  useEffect(() => {
    dispatch(moviesActions.fetch());
  }, [])

  const { data, refetch } = useQuery<MovieReviewProps>(GET_ALL_MOVIES_REVIEWS);

  useEffect(() => {
    if(data) {
      data.allMovieReviews.edges.map((movie: any) => {
        if(movie.node.movieId === review) {
          setMovie(movie.node)
        }
      })
    }
  }, [data])

  const handleSubmitEdit = () => {
    updateMovieReviewById({
      variables: {
        input: {
          movieReviewPatch: {
            body,
            title,
            rating: parseInt(rating)
          },
          id: editReviewId
        }
      },
    })

    handleClose();
    refetch();
  }

  const modalStyle = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: theme.palette.background.paper,
    border: `2px solid ${theme.palette.background.paper}`,
    boxShadow: 24,
    p: 4,
    display: 'flex',
    flexDirection: 'column',
  };

  return (
    <div css={styles.root}>
      <div css={styles.body}>
        <MovieCard 
          key={movieState.id || movie.movieByMovieId?.id}
          id={movieState.id || movie.movieByMovieId?.id}
          title={movieState.title || movie.movieByMovieId?.title}
          releaseDate={movieState.releaseDate || movie.movieByMovieId?.releaseDate}
          imgUrl={movieState.imgUrl || movie.movieByMovieId?.imgUrl}
          reviewAuthor={movieState.reviewAuthor || movie.movieByMovieId?.userByUserCreatorId?.name}
        />

        <Typography
          variant="h4"
          sx={{ 
            color: theme.palette.primary.light, 
            marginTop: 5,
            fontWeight: 'bold',
            fontSize: 30
          }}
        >
          Users reviews:
        </Typography>
        {
          data?.allMovieReviews.edges.map((movie: any) => {
            if(movie.node.movieId === review) {
              return (   
                <Box key={movie.node.id}>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                      marginTop: 5
                    }}
                  >
                    <PersonIcon sx={{ color: theme.palette.primary.main }} />
                    <Typography
                      sx={{
                        color: theme.palette.primary.light,
                        marginLeft: '0.5rem',
                        fontSize: '1.25rem',
                        fontWeight: 'bold',
                      }}
                    >{movie.node.userByUserReviewerId.name}</Typography>
                    <Box
                      sx={{
                        backgroundColor: theme.palette.background.default,
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        padding: '0.5rem',
                        borderRadius: '0.5rem',
                        cursor: 'pointer',
                        marginLeft: '1rem',

                        '&:hover': {
                          backgroundColor: theme.palette.grey[500],
                        },
                      }}
                      onClick={() => {
                        handleOpen()
                        setEditReviewId(movie.node.id)
                      }}
                    >
                      <Image 
                        src="/edit.svg" 
                        alt='edit'
                        width={24}
                        height={24}
                      />
                    </Box>
                  </Box>

                  <Modal
                    open={openModal}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                  >
                    <Box sx={modalStyle}>
                      <TextInputComponent 
                        state={title}
                        setState={setTitle}
                        label='Title of your review'
                        id='review-title'
                      />

                      <TextInputComponent 
                        state={rating}
                        setState={setRating}
                        label='Your rating'
                        id='rating'
                      />

                      <TextInputComponent 
                        state={body}
                        setState={setBody}
                        label='Your Movie Review'
                        id='movie-review'
                        isMovieReviewData
                      />

                      <Button
                        sx={{
                          marginTop: '1rem',
                          backgroundColor: theme.palette.primary.main,
                          color: theme.palette.primary.dark,
                          fontWeight: 'bold',
                          fontSize: '1rem',
                          padding: '0.5rem 1rem',
                          borderRadius: '0.5rem',
                          cursor: 'pointer',
                          width: '20%',
                          '&:hover': {
                            backgroundColor: theme.palette.secondary.main,
                          },
                        }}
                        onClick={handleSubmitEdit}
                      >
                        Submit Edit
                      </Button>
                    </Box>
                  </Modal>
                  


                  <Typography
                    sx={{
                      color: theme.palette.primary.light,
                      marginTop: '0.5rem',
                      fontSize: '1.25rem',
                      fontWeight: 'bold',
                    }}
                  >{movie.node.title}</Typography>
                  <Typography
                    sx={{
                      color: theme.palette.primary.light,
                      marginTop: '0.5rem',
                      fontSize: '1rem',
                      fontWeight: 'bold',
                    }}
                  >Rating: {movie.node.rating}</Typography>
                  <Typography
                    sx={{
                      color: theme.palette.primary.light,
                      marginTop: '0.5rem',
                      fontSize: '1rem',
                      fontWeight: 'bold',
                    }}
                  >Review: {movie.node.body}</Typography>
                </Box>
              )
            }
          })
        }
      </div>
    </div>
  );
};

const styles = {
  root: css({
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.grey[600],
  }),
  body: css({
    alignSelf: 'flex-start',
    width: 'fit-content',
    padding: 32,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  }),
};

export default MovieReviews;