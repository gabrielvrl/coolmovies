export const SET_MOVIE_PROPS = 'SET_MOVIE_PROPS';

export interface MovieReduxProps {
  id: string;
  title: string;
  releaseDate: string;
  reviewAuthor: string;
  imgUrl: string;
}

export const setMovieProps = (movieProps: MovieReduxProps) => {
  return {
    type: SET_MOVIE_PROPS,
    payload: {
      movieProps
    }
  };
};
