import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ShortMovieProps } from '../../../components/MovieCard';
import { MovieReduxProps } from '../../actions';

interface MoviesState {
  movieProps: ShortMovieProps;
  value: number;
  sideEffectCount: number;
  fetchData?: unknown[];
}

const initialState: MoviesState = {
  value: 0,
  sideEffectCount: 0,
  movieProps: {
    id: '',
    title: '',
    releaseDate: '',
    reviewAuthor: '',
    imgUrl: '',
  },
};

export const slice = createSlice({
  initialState,
  name: 'movies',
  reducers: {
    fetch: () => {},
    clearData: (state) => {
      state.fetchData = undefined;
    },
    loaded: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchData = action.payload.data;
    },
    loadError: (state) => {
      state.fetchData = ['Error Fetching :('];
    },
    increment: (state) => {
      state.value += 1;
    },
    epicSideEffect: (state) => {
      state.sideEffectCount += 1;
    },
    setMovieProps: (state = initialState, action: PayloadAction<MovieReduxProps>) => {
      state.movieProps = action.payload;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;