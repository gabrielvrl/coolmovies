import { createTheme } from "@mui/material";

export const theme = createTheme({
  palette: {
    primary: {
      main: '#FAE800',
      light: '#fff',
      dark: '#000',
    },
    secondary: {
      main: '#c2b310',
      light: '#FBFBFB',
    },
    background: {
      default: '#373945',
      paper: '#2E303C',
    },
    grey: {
      500: '#4B4D59',
      600: '#1F2229'
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
});
