import axios from 'axios';

const graphqlUrl = process.env.NEXT_PUBLIC_GRAPHQL_URL;

const api = axios.create({
  baseURL: graphqlUrl,
});

export default api;