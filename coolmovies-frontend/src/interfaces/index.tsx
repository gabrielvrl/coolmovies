export interface MoviesData {
  allMovies: {
    nodes: MovieProps[];
  };
}

export interface MovieProps {
  id: string;
  imgUrl: string;
  movieDirectorId: string;
  nodeId: string;
  releaseDate: string;
  title: string;
  userByUserCreatorId: UserByUserCreatorIdProps;
  userCreatorId: string;
  __typename: string;
}

interface UserByUserCreatorIdProps {
  id: string;
  name: string;
  nodeId: string;
  __typename: string;
}