import Image from 'next/image';
import {
  Box,
  Typography,
  useTheme,
} from '@mui/material';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { moviesActions } from '../redux';
import SelectActionComponent from './SelectActionComponent';

export interface ShortMovieProps {
  id: string;
  title: string;
  releaseDate: string;
  reviewAuthor: string;
  imgUrl: string;
}

const MovieCard = (movie: ShortMovieProps) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const theme = useTheme();

  const redirectToReview = useCallback(() => {
    dispatch(moviesActions.setMovieProps(movie));

    router.push(`/review/${encodeURIComponent(movie.id)}`);
  }, [dispatch])

  return(
    <Box
      sx={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: '1rem',
        position: 'relative',
        [theme.breakpoints.down('md')]: {
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        },
      }}
    >
      <Image
        src={movie?.imgUrl || '/placeholder.png'}
        alt={movie.title || 'Movie Image'}
        width={259}
        height={384}
        style={{ borderRadius: '1rem', marginRight: '1rem', cursor: 'pointer' }}
        onClick={redirectToReview}
      />
      
      <Box
        sx={{
          
          [theme.breakpoints.down('md')]: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          },
        }}
      >
        <Typography color={theme.palette.secondary.light} fontSize='1.5rem' fontWeight='bold'
          sx={{
            [theme.breakpoints.down('md')]: {
              fontSize: '1rem',
            },
          }}
        >{movie.title}</Typography>
        <Typography color={theme.palette.secondary.light}>Release Date: <strong>{movie.releaseDate}</strong></Typography>

        <SelectActionComponent movieId={movie.id} buttonTitle="See All Reviews" />
        <SelectActionComponent movieId={movie.id} buttonTitle="Add Movie Review" isAddMovieReview={true} />
      </Box>

      
    </Box>
  );
};

export default MovieCard;
