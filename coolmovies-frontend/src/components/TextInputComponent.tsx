import { TextField } from "@mui/material";
import { useTheme } from '@mui/material/styles';

interface TextInputProps {
  state: string;
  setState: (state: string) => void;
  label: string;
  id: string;
  isMovieReviewData?: boolean;
}

const TextInputComponent = ({ state, setState, label, id, isMovieReviewData }: TextInputProps) => {
  const theme = useTheme();
  const width = isMovieReviewData ? 500 : 200;

  return(
    <TextField id={id} label={label} variant="standard"  sx={{
      marginBottom: '1rem',
      color: theme.palette.primary.light,
      backgroundColor: theme.palette.background.default,
      borderRadius: '0.2rem',
      '& label': {
        color: theme.palette.primary.main,
      },
      '& input': {
        color: theme.palette.primary.light,
        fontWeight: 'bold',
      },
      width: {width},
      [theme.breakpoints.down('md')]: {
        width: 300,
      },
    }} value={state} onChange={e => setState(e.target.value)} />
  )
};

export default TextInputComponent;