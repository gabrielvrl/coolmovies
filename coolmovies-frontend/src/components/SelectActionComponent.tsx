import { Box, Typography, useTheme } from "@mui/material";
import { useRouter } from "next/router";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';

interface SelectActionComponentProps {
  movieId: string;
  buttonTitle: string;
  isAddMovieReview?: boolean;
}

const SelectActionComponent = ({ movieId, buttonTitle, isAddMovieReview }: SelectActionComponentProps) => {
  const theme = useTheme();
  const router = useRouter();

  const backgroundColor = isAddMovieReview ? theme.palette.grey[500] : theme.palette.primary.main;
  const hoverColor = isAddMovieReview ? theme.palette.background.default : theme.palette.secondary.main;

  return(
    <Box
      sx={{
        backgroundColor: backgroundColor,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0.25rem',
        borderRadius: '0.5rem',
        cursor: 'pointer',
        width: '250px',
        margin: '1.5rem 0',
        gap: '4px',

        '&:hover': {
          backgroundColor: hoverColor,
        },
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent: 'center',
          padding: '0.5rem',
          borderRadius: '0.5rem',
          cursor: 'pointer',
        }}
        onClick={() => {
          isAddMovieReview ? router.push(`/movie/${encodeURIComponent(movieId)}`) : router.push(`/review/${encodeURIComponent(movieId)}`)
        }}
        
      >
        <Typography
          sx={{
            marginRight: '0.25rem',
            fontSize: '1.05rem',
          }}
        >
          {buttonTitle}
        </Typography>
        {
          isAddMovieReview ? <AddCircleIcon /> :
          <RemoveRedEyeIcon />
        }
      </Box>
    </Box>
  )
}

export default SelectActionComponent;