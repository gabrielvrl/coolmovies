import {
  Box,
  Typography,
  useTheme,
} from '@mui/material';
import { useRouter } from 'next/router';

const Header = () => {
  const router = useRouter();
  const theme = useTheme();
    
  return(
    <Box
      sx={{
        width: '100%',
        maxHeight: '50%',
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '1rem',
      }}
    >
      <Typography
        sx={{
          color: theme.palette.primary.main,
          fontSize: '3rem',
          fontWeight: 'bold',
          padding: '1rem',
          cursor: 'pointer',
          borderRadius: '0.5rem',
        }}
        onClick={() => router.push('/')}
      >
        CoolMovies
      </Typography>
    </Box>
  );
};

export default Header;
